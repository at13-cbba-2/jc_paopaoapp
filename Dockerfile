FROM openjdk:11-jre-slim

EXPOSE 8080
WORKDIR /paopao
COPY ./.env.develop /paopao/.env.develop
COPY ./archive /paopao/archive
COPY ./build/libs/*.jar /paopao/converter-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java", "-jar", "converter-0.0.1-SNAPSHOT.jar" ]
